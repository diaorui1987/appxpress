<?php
 require_once("Rest.inc.php");

 class API extends REST {
  public $data = "";

  const DB_SERVER = "";
  const DB_SERVER_PORT = "";
  const DB_USER = "";
  const DB_PASSWORD = "";
  const DB = "";

  private $db = NULL;
  private $mysqli = NULL;
  public function __construct() {
    parent::__construct();
    $this->dbConnect();
  }


  private function dbConnect() {
    $this->mysqli = new mysqli( self::DB_SERVER, self::DB_USER, self::DB_PASSWORD, self::DB, self::DB_SERVER_PORT );
  }



  public function processApi() {
    $func = strtolower(trim(str_replace("/","",$_REQUEST['x'])));
    if((int)method_exists($this,$func) > 0 )
      $this->$func();
    else
      $this->ver();
      //$this->response('',404);
  }

  private function ver() {
    if($this->get_request_method() != "GET"){
      $this->response('',406);
    }
    $verArray = array( "version" => "1.0" );
    $this->response($this->json($verArray), 200);
  }

  private function dbinfo() {
    if($this->get_request_method() != "GET"){
      $this->response('',406);
    }
    $this->response($this->json( $mysqli->host_info ), 200);
  }


  private function submit() {
    if($this->get_request_method() != "POST"){
      $this->response('',406);
    }


    $submission = json_decode(file_get_contents("php://input"),true);

    $column_names = array(
	"appDescription",
	"appGoal",
	"appType",
	"appUserDesc",
	"appMainFeatures",
	"appUserFlow",
	"Device",
	"OSes",
	"noOfStaticPages",
	"uploadedFiles"
	);
    $keys = array_keys($submission);
    $columns = '';
    $values = '';
    foreach( $column_names as $desired_key ) {
      if ( !in_array( $desired_key, $keys )) {
          $$desired_key = '';  // default to blank
      }
      else {
        if(is_array($submission[$desired_key])){
          $$desired_key =  json_encode($submission[$desired_key] );
        }
        else {
          $$desired_key =  json_encode($submission[$desired_key] );
          //$$desired_key =  json_encode($submission[$desired_key], JSON_HEX_APOS | JSON_HEX_QUOT );
          //$$desired_key = $submission[$desired_key];
        }
      }
      $$desired_key = str_replace("'", "''", $$desired_key);
      $columns = $columns.$desired_key.',';
      $values = $values."'".$$desired_key."',";
    }

    $query = "INSERT INTO submissions(".trim($columns,',').") VALUES(".trim($values,',').")";
    if (!empty($submission)) {
      $r = $this->mysqli->query($query) or die($this->mysqli->error.__LINE__);
      $success = array('status' => "Success", "ordernumber" => $this->mysqli->insert_id );
      $this->response($this->json($success),200);
    }
    else {
      $this->response($this->json($submission),200);
    }
  }

private function retrieve() {
	if ($this->get_request_method() != "POST" ) {
		$this->response('',406);
	}

  $params = json_decode(file_get_contents("php://input"),true);
  if ( $params["apx_id"] ) {

    $query = "SELECT * FROM submissions WHERE ordernumber=" . $params["apx_id"] . " LIMIT 1"; 
    $r = $this->mysqli->query($query) or die($this->mysqli->error.__LINE__);
    if ( $r->num_rows > 0 ) {
      $this->response($this->json($r->fetch_object()),200);
    }
    else {
      $this->response("No records",200);
    }
  }
  else {
      $this->response('',406);
  }
}


  private function upload() {
    if($this->get_request_method() != "POST"){
      $this->response('',406);
    }
    if ( !empty( $_FILES ) ) {

       $tempPath = $_FILES[ 'file' ][ 'tmp_name' ];
       $uploadPath = dirname( __FILE__ ) . '/' . 'uploads' . '/' . $_FILES[ 'file' ][ 'name' ];

       move_uploaded_file( $tempPath, $uploadPath );

      $success = array('status' => "Success", "message" => $tempPath  );
      $this->response($this->json($success),200);
    }
    else {
      $this->response('',406);
    }
}


  private function json($data) {
    if(is_array($data)){
      return json_encode($data);
    }
    else {
      return json_encode( array("response" => $data ));
    }
  }
 }
  $api = new API;
  $api->processApi();
