<?php
if ( ! defined( 'ABSPATH' ) ) exit;
/**
 * Template Name: AngujarJS Test
 *
 * @package WooFramework
 * @subpackage Template
 */
	 get_header(); 
	 global $woo_options; 
?>
      <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700,300,600" rel="stylesheet" type="text/css">
      <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
      <link href="/styles/bootstrap.css" rel="stylesheet" type="text/css">
      <link href="/styles/screen.css" rel="stylesheet" type="text/css">
      <link href="/styles/textAngular.css" rel="stylesheet" type="text/css">

    <div class="container" style="color:black">
      <div ui-view>
      </div>
    </div>

    <script data-main="/scripts/main" src="/bower_components/requirejs/require.js"></script>

<?php get_footer();  ?>
