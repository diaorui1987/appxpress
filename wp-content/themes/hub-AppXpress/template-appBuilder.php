<?php
if ( ! defined( 'ABSPATH' ) ) exit;
/**
 * Template Name: Appbuilder use only
 *
 * This template is a full-width version of the page.php template file. It removes the sidebar area.
 *
 * @package WooFramework
 * @subpackage Template
 */
	get_header(); 
	global $woo_options;
?>
      <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700,300,600" rel="stylesheet" type="text/css">
      <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
      <link href="../styles/main.css" rel="stylesheet" type="text/css">

    <div ui-view>
    </div>

<?php   get_footer( "appbuilder" ); ?>
<script data-main="../scripts/main" src="../bower_components/requirejs/require.js"></script>
</body>
</html>
