<?php
if ( ! defined( 'ABSPATH' ) ) exit;
/**
 * Header Template
 *
 * Here we setup all logic and XHTML that is required for the header section of all screens.
 *
 * @package WooFramework
 * @subpackage Template
 */

 global $woo_options, $woocommerce;

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<title><?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 */
	global $page, $paged;

	wp_title( '|', true, 'right' );

	// Add the blog name.
	//bloginfo( 'name' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";

	// Add a page number if necessary:
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( __( 'Page %s', 'woothemes' ), max( $paged, $page ) );

	?></title>
<?php woo_meta(); ?>
<link rel="pingback" href="<?php echo esc_url( get_bloginfo( 'pingback_url' ) ); ?>" />
<?php
wp_head();
woo_head();
?>
<script type='text/javascript' src='<?php echo get_bloginfo( 'stylesheet_directory' );?>/js/owl.carousel.js'></script>
<script type='text/javascript' src='<?php echo get_bloginfo( 'stylesheet_directory' );?>/js/placeholders.min.js'></script>
<script type='text/javascript' src='<?php echo get_bloginfo( 'stylesheet_directory' );?>/js/script.js'></script>
<link href="<?php echo get_bloginfo( 'stylesheet_directory' );?>/styles/owl.theme.css" rel="stylesheet" type="text/css" />
<link href="<?php echo get_bloginfo( 'stylesheet_directory' );?>/styles/owl.carousel.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet/css" type="text/css" href="<?php echo get_bloginfo( 'stylesheet_directory' );?>/styles/default.css" />


<!-- HotJar -->
<script>
    (function(f,b,g){
        var xo=g.prototype.open,xs=g.prototype.send,c;
        f.hj=f.hj||function(){(f.hj.q=f.hj.q||[]).push(arguments)};
        f._hjSettings={hjid:5072, hjsv:2};
        function ls(){f.hj.documentHtml=b.documentElement.outerHTML;c=b.createElement("script");c.async=1;c.src="//static.hotjar.com/c/hotjar-5072.js?sv=2";b.getElementsByTagName("head")[0].appendChild(c);}
        if(b.readyState==="interactive"||b.readyState==="complete"||b.readyState==="loaded"){ls();}else{if(b.addEventListener){b.addEventListener("DOMContentLoaded",ls,false);}}
        if(!f._hjPlayback && b.addEventListener){
            g.prototype.open=function(l,j,m,h,k){this._u=j;xo.call(this,l,j,m,h,k)};
            g.prototype.send=function(e){var j=this;function h(){if(j.readyState===4){f.hj("_xhr",j._u,j.status,j.response)}}this.addEventListener("readystatechange",h,false);xs.call(this,e)};
        }
    })(window,document,window.XMLHttpRequest);
</script>
<!-- end HotJar -->
</head>
<body <?php body_class(); ?>>
<?php woo_top(); ?>

<div id="wrapper">
	<div class="mobileWrapper">

    <?php woo_header_before(); ?>
<div id="header-main">
	<div id="inner-wrapper">	
    <div id="header-wrapper">

			<div class="wrapper">

				<header id="header">

						<?php woo_header_inside(); ?>
						
						<span class="logoTitle"><?php echo get_option("woo_head_title","AppXpress Marketplace"); ?></span>
						
						<span class="nav-toggle">
							<a href="#navigation"><span><?php _e( 'Navigation', 'woothemes' ); ?></span></a>
						</span>

						<div class="site-header">
							<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php bloginfo( 'name' ); ?></a></h1>
							<h2 class="site-description"><?php bloginfo( 'description' ); ?></h2>
						</div>

								<?php woo_nav_before(); ?>

						<nav id="navigation" role="navigation">

							<section class="menus">

							<a href="<?php echo home_url(); ?>" class="nav-home"><span><?php _e( 'Home', 'woothemes' ); ?></span></a>

							<?php if ( is_woocommerce_activated() && isset( $woo_options['woocommerce_header_cart_link'] ) && 'true' == $woo_options['woocommerce_header_cart_link'] ) { ?>
										<h3><?php _e( 'Shopping Cart', 'woothemes' ); ?></h3>
										<ul class="nav cart">
											<li <?php if ( is_cart() ) { echo 'class="current-menu-item"'; } ?>>
												<?php woo_wc_cart_link(); ?>
											</li>
										</ul>
									<?php }
							if ( function_exists( 'has_nav_menu' ) && has_nav_menu( 'primary-menu' ) ) {
								echo '<h3>' . woo_get_menu_name('primary-menu') . '</h3>';

if ( is_user_logged_in() ) {
	wp_nav_menu( array( 'depth' => 6, 
											'sort_column' => 'menu_order', 
                      'container' => 'ul', 
											'menu_id' => 'main-nav', 
											'menu_class' => 'nav', 
											'theme_location' => 'primary-menu' 
		) 
	);
}
else {
	wp_nav_menu( array( 'depth' => 6, 
											'sort_column' => 'menu_order', 
											'container' => 'ul', 
											'menu_id' => 'main-nav',
											'menu_class' => 'nav', 
											'theme_location' => 'primary-menu',
											'menu' => 'not-logged-in-nav'
		) 
	);
}
							} else {
							?>
									<ul id="main-nav" class="nav">
								<?php if ( is_page() ) $highlight = 'page_item'; else $highlight = 'page_item current_page_item'; ?>
								<li class="<?php echo $highlight; ?>"><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php _e( 'Home', 'woothemes' ); ?></a></li>
								<?php wp_list_pages( 'sort_column=menu_order&depth=6&title_li=&exclude=' ); ?>
							</ul><!-- /#nav -->
									<?php } ?>

								</section><!--/.menus-->

									<a href="#top" class="nav-close"><span><?php _e('Return to Content', 'woothemes' ); ?></span></a>

						</nav><!-- /#navigation -->
						<!--<div id="mobileNav"></div>-->
						<?php woo_nav_after(); ?>

				</header><!-- /#header -->

			</div><!-- /.wrapper -->
		</div><!-- /#header-out -->
	</div>
</div>

	<?php woo_content_before(); ?>


