<?php
if ( ! defined( 'ABSPATH' ) ) exit;
/**
 * Template Name: Appbuilder use only Num 2
 *
 * This template is a full-width version of the page.php template file. It removes the sidebar area.
 *
 * @package WooFramework
 * @subpackage Template
 */
	get_header(); 
	global $woo_options;
?>
      <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700,300,600" rel="stylesheet" type="text/css">
      <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
      <link href="../styles/bootstrap.css" rel="stylesheet" type="text/css">
      <link href="../styles/screen.css" rel="stylesheet" type="text/css">
      <link href="../styles/textAngular.css" rel="stylesheet" type="text/css">
    <div ui-view>
    </div>

    <script>
            var BRANDING = "APPIRIO";
    </script>


<?php   get_footer();  ?>

<script data-main="../scripts/main" src="../bower_components/requirejs/require.js"></script>

