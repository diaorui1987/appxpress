<?php
if ( ! defined( 'ABSPATH' ) ) exit;

get_header();
?>

    <div id="content" class="col-full">

        <div class="wrapper">        

            <?php woo_main_before(); ?>

            <section id="main" class="col-left">

                <div class="page type-page">

                    <header>
                        <h2>Looking for something?</h2>
                    </header>
                    <section class="entry">
<p>Unfortunately, there is no information at this location.</p>

<p>You may have been looking for our main website, which is <a href="/">located here.</a></p>

<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>

                    </section>

                </div><!-- /.page -->

            </section><!-- /#main -->

            <?php woo_main_after(); ?>

            <?php //get_sidebar(); ?>

        </div><!-- /.wrapper -->        

    </div><!-- /#content -->

<?php get_footer(); ?>
