<?php
/*
Template Name: Featured Page
*/

// File Security Check
if ( ! function_exists( 'wp' ) && ! empty( $_SERVER['SCRIPT_FILENAME'] ) && basename( __FILE__ ) == basename( $_SERVER['SCRIPT_FILENAME'] ) ) {
    die ( 'You do not have sufficient permissions to access this page!' );
}
?><?php
/**
 * Index Template
 *
 * Here we setup all logic and XHTML that is required for the index template, used as both the homepage
 * and as a fallback template, if a more appropriate template file doesn't exist for a specific context.
 *
 * @package WooFramework
 * @subpackage Template
 */
	get_header();
	global $woo_options;
	if(have_posts()) :
		the_post();
		
		// Custom post field for alternative page title
		$custom_title = get_post_meta( get_the_ID(), 'alt_title', true );
?>

<div id="featuredWrapper" class="fullWrapper">
	<div class="inner-wrapper">
		<div class="mainContentWrapper">
			<h3 class="greyTitle mainContentTitle"><?php if ($custom_title!='') { 
				echo $custom_title; 
			} else {
				the_title();
			}
			?></h3>
			<div class="mainContentGeneric genericContent">
				<?php the_content();?>
			</div>
		</div>
	</div>
</div>

<div id="features">
	<ul class="slides">
	<?php
			$args = "post_type=".FEATURE;
			$args .= "&order=ASC&orderby=menu_order";
			$i=1;
			wp_reset_query();
			query_posts($args);
			if ( have_posts() ) :
				while(have_posts() ) : the_post();
					$postId = $post->ID;
					$thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ) , "full" );
					$thumbnailSrc = $thumbnail != null ? $thumbnail[0] : null;
		?>
		<li class="fullWrapper featuredItem">
			<div class="inner-wrapper">
			
				<h4 class="greyTitle"><?php the_title();?></h4>
				<div class="imagePlaceholder">
					<?php if($thumbnailSrc!=null) : ?>
						<img src="<?php echo $thumbnailSrc;?>" alt="" />
					<?php else : ?>
						<img src="<?php echo get_bloginfo( 'stylesheet_directory' );?>/images/laptop2.png" alt="" />
					<?php endif; ?>
				</div>
				<div class="featureContent genericContent">
					<div class="featureContentInner">
						<?php the_content();?>
					</div>
				</div>
				<div class="floatFix"></div>
			
			</div>
		</li>
	<?php
			endwhile;
		endif;
	?>

	</ul>
</div>

<div id="featuredSlider">
	<ul class="slides">
	<?php
		$args = "post_type=".FEATURE;
		$args .= "&order=ASC&orderby=menu_order";
		$i=1;
		wp_reset_query();
		query_posts($args);
		if ( have_posts() ) :
			while(have_posts() ) : the_post();
				$postId = $post->ID;
				$thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ) , "full" );
				$thumbnailSrc = $thumbnail != null ? $thumbnail[0] : null;
	?>
		<li class="fullWrapper featuredItem">
			<div class="inner-wrapper">
			
				<h4 class="greyTitle"><?php the_title();?></h4>
				<div class="imagePlaceholder">
					<?php if($thumbnailSrc!=null) : ?>
						<img src="<?php echo $thumbnailSrc;?>" alt="" />
					<?php else : ?>
						<img src="<?php echo get_bloginfo( 'stylesheet_directory' );?>/images/laptop2.png" alt="" />
					<?php endif; ?>
				</div>
				<div class="featureContent genericContent">
					<?php the_content();?>
				</div>
				<div class="floatFix"></div>
			
			</div>
		</li>
	<?php
			endwhile;
		endif;
	?>

	</ul>
</div>
<?php endif;?>
<?php get_footer(); ?>
