<?php
if ( ! defined( 'ABSPATH' ) ) exit;

/*-----------------------------------------------------------------------------------*/
/* Start WooThemes Functions - Please refrain from editing this section */
/*-----------------------------------------------------------------------------------*/

// WooFramework init
require_once ( get_template_directory() . '/functions/admin-init.php' );

/*-----------------------------------------------------------------------------------*/
/* Load the theme-specific files, with support for overriding via a child theme.
/*-----------------------------------------------------------------------------------*/

$includes = array(
				'includes/theme-options.php', 				// Options panel settings and custom settings
				'includes/theme-functions.php', 			// Custom theme functions
				'includes/theme-actions.php', 				// Theme actions & user defined hooks
				'includes/theme-comments.php', 				// Custom comments/pingback loop
				'includes/theme-js.php', 					// Load JavaScript via wp_enqueue_script
				'includes/sidebar-init.php', 				// Initialize widgetized areas
				'includes/theme-widgets.php',				// Theme widgets
				'includes/theme-plugin-integrations.php'	// Plugin integrations
				);

// Allow child themes/plugins to add widgets to be loaded.
$includes = apply_filters( 'woo_includes', $includes );

foreach ( $includes as $i ) {
	locate_template( $i, true );
}

/*-----------------------------------------------------------------------------------*/
/* You can add custom functions below */
/*-----------------------------------------------------------------------------------*/
function image_shortcode( $atts , $content="" ){
	$ret = get_bloginfo( 'stylesheet_directory' )."/images/".$content;
	return $ret;
}
add_shortcode( 'imageDir', 'image_shortcode' );

function baseUrl( $atts, $content="" ){
	$ret = get_bloginfo( 'wpurl' )."/".$content;
	return $ret;
}
add_shortcode( 'baseUrl', 'baseUrl' );
remove_filter( 'the_content', 'wpautop' );

/*  Add filter according to lesson 1 from
   http://docs.woothemes.com/document/tutorial-customising-checkout-fields-using-actions-and-filters/
*/

add_filter( 'woocommerce_checkout_fields', 'custom_override_checkout_fields' );

// our hooked function.  $fields is passed via the filter
function custom_override_checkout_fields( $fields ) {
	$fields['order']['order_comments']['placeholder'] = 
		'Anything else you want to tell us?';
	return $fields;
}

/*  function to override permalink for products */
add_filter( 'appxpress_cart_item_product_url', 'appxpress_cart_item_product_url_func' );
function appxpress_cart_item_product_url_func( $url ) {
	if ( strpos($url, 'design-mockups') !== FALSE ){ 
	   return '/appbuilder2/#protoFlow/1';
	}
	if ( strpos($url, 'pilot-application') !== FALSE ) { 
	   return '/appbuilder2/#pilotFlow/1';
        }
	if ( strpos($url, 'production-application') !== FALSE ) { 
	   return '/appbuilder2/#productFlow/1';
        }
	  return $url;
         
}


/**
 * Add to extended_valid_elements for TinyMCE
 *
 * @param $init assoc. array of TinyMCE options
 * @return $init the changed assoc. array
 */
function my_change_mce_options( $init ) {
    // Command separated string of extended elements
    $ext = 'pre[id|name|class|style]';

    // Add to extended_valid_elements if it alreay exists
    if ( isset( $init['extended_valid_elements'] ) ) {
        $init['extended_valid_elements'] .= ',' . $ext;
    } else {
        $init['extended_valid_elements'] = $ext;
    }

    // Super important: return $init!
    return $init;
}

add_filter('tiny_mce_before_init', 'my_change_mce_options');

define("FEATURE","feature");

/* Promo Module Post Type */
add_action('init', 'promo_register');
function promo_register() {

	$strPostName = 'Feature';

	$labels = array(
		'name'					=> _x($strPostName.'s', 'post type general name'),
		'singular_name'			=> _x($strPostName, 'post type singular name'),
		'add_new'				=> _x('Add New', $strPostName.' Post'),
		'add_new_item'			=> __('Add New '.$strPostName.' Post'),
		'edit_item'				=> __('Edit '.$strPostName.' Post'),
		'new_item'				=> __('New '.$strPostName.' Post'),
		'view_item'				=> __('View '.$strPostName.' Post'),
		'search_items'			=> __('Search '.$strPostName),
		'not_found'				=> __('Nothing found'),
		'not_found_in_trash'	=> __('Nothing found in Trash'),
		'parent_item_colon'		=> ''
	);

	$args = array(
		'labels'				=> $labels,
		'public'				=> true,
		'publicly_queryable'	=> true,
		'show_ui'				=> true,
		'query_var'				=> true,
		'rewrite'				=> true,
		'capability_type'		=> 'post',
		'hierarchical'			=> false,
		'menu_position'			=> 5,
		'exclude_from_search'	=> false,
		'show_in_nav_menus'		=> true,
		'supports'				=> array('title', 'editor', 'thumbnail', 'page-attributes')
	  );

	register_post_type( 'feature' , $args );


	flush_rewrite_rules( false );
}



/*  ----
  Custom shortcodes for appbuilder
*/

function appbuilder_func( $atts ) {
	$a = shortcode_atts( array(
		'brand' => 'appirio'
	     ), $atts);

	/*  $a['brand'] == 'appirio' */
	/*  $a['brand'] == 'coke' */

	ob_start();
	?>

      <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700,300,600" rel="stylesheet" type="text/css">
      <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
      <link href="/styles/bootstrap.css" rel="stylesheet" type="text/css">
      <link href="/styles/screen.css" rel="stylesheet" type="text/css">
      <link href="/styles/textAngular.css" rel="stylesheet" type="text/css">
	<div ui-view="">
    	</div>

    <script>
      var BRANDING = "APPIRIO";
    </script>
    <script data-main="/scripts/main" src="/bower_components/requirejs/require.js"></script>

	<?PHP
	return ob_get_clean();	
}

add_shortcode( 'appbuilder', 'appbuilder_func' );


/*
  One item in the cart at a time
*/

/*add_filter( 'woocommerce_add_cart_item_data', 'woocommerce_add_cart_item_data_func' );

function woocommerce_add_cart_item_data_func( $cart_item_data ) { 
	// Empty the existing cart
	global $woocommerce;
	$woocommerce->cart->empty_cart();
	// Do nothing with the data, just return it
	// this will add the new item to the cart
	return $cart_item_data;
}
*/

/*  
  Custom field added to the order
*/

add_action( 'woocommerce_after_add_to_cart_button', 'addFieldToFrontEnd', 10, 2 );


// add to cart
add_filter( 'woocommerce_add_cart_item', 'axp_add_cart_item', 10, 1 );

// add item data to the cart
add_filter( 'woocommerce_add_cart_item_data', 'axp_add_cart_item_data', 10, 2 );

// Load cart data per page load
add_filter( 'woocommerce_get_cart_item_from_session', 'axp_get_cart_item_from_session', 10, 2 );

// validate when adding to cart
//  woocommerce_add_to_cart_validate

// ??
add_filter( 'woocommerce_get_item_data', 'axp_get_item_data', 10, 2 );

// Add meta to order ( woo 2.x  ) 
add_filter( 'woocommerce_add_order_item_meta', 'axp_add_order_item_meta', 10, 2 );



/*  Create a field so that we can pass axp_id to the cart */
function addFieldToFrontEnd() {
  global $post;
  $current_value = ! empty( $_REQUEST['axp_id'] ) ? 1 : 0;
  $inpbox = "<input type='hidden' name='axp_id' id='axp_id' value='' />";
  echo $inpbox;
}

function axp_add_cart_item( $cart_item ) {
	// Empty the existing cart
	global $woocommerce;
	$woocommerce->cart->empty_cart();
	// Do nothing with the data, just return it
	// this will add the new item to the cart
	return $cart_item;
}

// Save the axp_id to the cart item meta
function axp_add_cart_item_data( $cart_item_meta, $product_id ) { 
	global $woocommerce;
	$woocommerce->cart->empty_cart();

  $axp_id = $_COOKIE["axp_id"];
  if ( $axp_id == '' ) { 
    $axp_id = 'unknown';
  }
/*
	$axp_id = $_POST['axp_id'];
	if ( $axp_id == '' ) {
		$axp_id = $_GET['axp_id'];
		if ( $axp_id == '' ) {
     
			
			$axp_id = 'default';
		}
	}
*/
	$cart_item_meta['axp_id'] = $axp_id;
	return $cart_item_meta;
}


function axp_get_cart_item_from_session($cart_item, $values ) {
	//$cart_item['axp_id'] = $values['axp_id'];
	$cart_item['axp_id'] = $_COOKIE["axp_id"];
  return $cart_item;
}


function axp_get_item_data( $item_data, $cart_item ) {
	$item_data[] = array( 
		"name" => __( 'AXP Details', 'axp_id' ),
	  "value" => __( $cart_item['axp_id'], $cart_item['axp_id'] ),
	  "display" => __( '<a href="/appbuilder2/order/' . $cart_item['axp_id'] . '">Details</a>', 
	 		 	$cart_item['axp_id'] )
	);
	return item_data;
}



function axp_add_order_item_meta( $item_id, $cart_item ) {
  $axp_id = $_COOKIE["axp_id"];
  woocommerce_add_order_item_meta( 
	$item_id, 
		__('axp_id', 'axp_id'), 
		//__( $cart_item['axp_id'], $cart_item['axp_id'] )
		__( $axp_id, $axp_id )
  );
}



add_filter( 'wwoocommerce_order_item_display_meta_valu', 'axp_order_item_display', 10, 1 );
function axp_order_item_display($item) {
	return '<a href="/appbuilder2/order/' . $item . '">Details</a>';
}

add_action( 'woocommerce_order_item_line_item_html', 'my_order_item_line_item_html', 10, 2);
function my_order_item_line_item_html( $item_id, $item ) {
  echo '<div style="padding-left:15px">';
  echo "Item Details: <a href='/appbuilder2/#/viewSummary/" . $item['axp_id'] . "' target='_blank'>View Summary</a>";
  echo '</div>';
  
}

add_action( 'woocommerce_proceed_to_checkout', 'validate_protoFlow_completed_adderror' );
function validate_protoFlow_completed_adderror() {
  // Check if cookie is set and valid, if it's not then throw an alert.
  if ( !(isset($_COOKIE['axp_id']) && $_COOKIE['axp_id'] != "undefined") ) {
     echo '<script language="javascript">';
     echo 'alert("Oops, we need to fix a few things. You\'ll need to select \'Edit Order\' below and complete the submission of your order details before you can checkout.")';
     echo '</script>';
  }
}

add_action( 'woocommerce_before_checkout_form', 'validate_protoFlow_completed_addalert' );
function validate_protoFlow_completed_addalert() {
  // Check if cookie is set and valid, if it's not then redirect back to shoppingcart.
  if ( !(isset($_COOKIE['axp_id']) && $_COOKIE['axp_id'] != "undefined") ) {
     global $woocommerce;
     $cart_url = $woocommerce->cart->get_cart_url();
     wp_safe_redirect($cart_url);
  }
}

/*-----------------------------------------------------------------------------------*/
/* Don't add any code below here or the sky will fall down */
/*-----------------------------------------------------------------------------------*/
?>
