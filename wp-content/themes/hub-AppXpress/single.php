<?php
if ( ! defined( 'ABSPATH' ) ) exit;
/**
 * Single Post Template
 *
 * This template is the default page template. It is used to display content when someone is viewing a
 * singular view of a post ('post' post_type).
 * @link http://codex.wordpress.org/Post_Types#Post
 *
 * @package WooFramework
 * @subpackage Template
 */
	get_header();
	global $woo_options;
	if(have_posts()) :
		the_post();
?>

<div id="" class="fullWrapper">
	<div class="inner-wrapper">
		<div class="mainContentWrapper">
			<h3 class="greyTitle mainContentTitle"><?php the_title();?></h3>
			<div class="mainContentGeneric genericContent">
				<?php the_content();?>
			</div>
		</div>
	</div>
</div>

<?php endif; ?>
<?php get_footer(); ?>