<?php
if ( ! defined( 'ABSPATH' ) ) exit;
/**
 * Template Name:  Blank (for AngularJS templates)
 *
 * 
 *
 * @package WooFramework
 * @subpackage Template
 */
global $woo_options;

woo_main_before(); 
the_post(); 
print get_the_content(); 
/*woo_main_after(); */

?>
