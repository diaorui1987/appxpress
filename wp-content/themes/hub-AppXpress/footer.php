<?php
if ( ! defined( 'ABSPATH' ) ) exit;
/**
 * Footer Template
 *
 * Here we setup all logic and XHTML that is required for the footer section of all screens.
 *
 * @package WooFramework
 * @subpackage Template
 */
	global $woo_options;

?>

	<div id="footer-wrapper">

<?php

	$total = 4;
	if ( isset( $woo_options['woo_footer_sidebars'] ) && ( '' != $woo_options['woo_footer_sidebars'] ) ) {
		$total = $woo_options['woo_footer_sidebars'];
	}

	if ( ( woo_active_sidebar( 'footer-1' ) ||
		   woo_active_sidebar( 'footer-2' ) ||
		   woo_active_sidebar( 'footer-3' ) ||
		   woo_active_sidebar( 'footer-4' ) ) && $total > 0 ) {

?>

		<?php woo_footer_before(); ?>

		<section id="footer-widgets">

			<div class="wrapper col-<?php echo esc_attr( $total ); ?> fix">

				<?php $i = 0; while ( $i < $total ) { $i++; ?>
					<?php if ( woo_active_sidebar( 'footer-' . $i ) ) { ?>

				<div class="block footer-widget-<?php echo $i; ?>">
		        	<?php woo_sidebar( 'footer-' . $i ); ?>
				</div>

			        <?php } ?>
				<?php } // End WHILE Loop ?>

			</div><!-- /.wrapper -->

		</section><!-- /#footer-widgets  -->
<?php } // End IF Statement ?>
		<footer id="footer">

			<div class="wrapper">

				<div class="appirioCopyright"><?php echo get_option("woo_copyright"); ?></div>
				
				
				
				<div class="socialFooter">
					<span><?php echo get_option("woo_trademark");?></span>
					<?php if(get_option("woo_twitter")!=null) :?><a target="_blank" href="<?php echo get_option("woo_twitter");?>" id="twitter"></a><?php endif;?>
					<?php if(get_option("woo_facebook")!=null) :?><a target="_blank" href="<?php echo get_option("woo_facebook");?>" id="fb"></a><?php endif;?>
				</div>
			<?php if(is_front_page()) : ?>	
			<!--
				<div class="stayInformed">
					<label>Stay Informed</label>
					<input type="text" placeholder="Your Email" />
					<a class="blueButton" href="javascript:;">OK</a>
				</div>
			-->
			<?php endif; ?>

			</div><!-- /.wrapper -->

		</footer><!-- /#footer  -->

	</div><!-- /#footer-wrapper -->

	</div><!-- /#mobileWrapper -->
</div><!-- /#wrapper -->
<?php wp_footer(); ?>
<?php woo_foot(); ?>
</body>
</html>
