(function (define) {
    "use strict";
    define(
        ['angular','jQuery'], function (angular) {

            var blurFocusDirective = function () {
                return {
                    restrict: 'E',
                    require: '?ngModel',
                    link: function (scope, elm, attr, ctrl) {
                        if (!ctrl) {
                            return;
                        }

                        elm.on('focus', function () {
                            elm.addClass('has-focus');

                            scope.$apply(function () {
                                ctrl.hasFocus = true;
                            });
                        });

                        elm.on('blur', function () {
                            elm.removeClass('has-focus');
                            elm.addClass('has-visited');

                            scope.$apply(function () {
                                ctrl.hasFocus = false;
                                ctrl.hasVisited = true;
                            });
                        });

                        elm.closest('form').on('submit', function () {
                            elm.addClass('has-visited');

                            scope.$apply(function () {
                                ctrl.hasFocus = false;
                                ctrl.hasVisited = true;
                            });
                        });

                    }
                };
            };




            angular.module('AppXpress.directives', [])
                .directive('printWindow',['$window',function($window){
                    return{
                        restrict: 'A',
                        link :function(scope, elem){
                            elem.click(function(){
                                $window.print();
                            });
                        }

                    }
                }])
                .directive('triggerEvent',[function(){
                    return{
                        restrict: 'A',
                        link :function(scope, elem, attrs){
                            elem.on(attrs.event,function(){
                                angular.element(attrs.triggerElement).trigger('click');
                            });
                        }

                    }



                }])
                .directive('navbarToggle',['$window',function($window){
                    return{
                        restrict: 'A',
                        link :function(scope, elem){
                            angular.element('.navbar-toggle').on('click',function(event){
                                if(angular.element('.slider-panel').hasClass('panel-open')){
                                    angular.element('.slider-panel').removeClass('panel-open');
                                    angular.element('.wrapper').removeClass('wrapper-active');
                                    angular.element('body').removeClass('window-noscroll');
                                }else{
                                    angular.element('.slider-panel').addClass('panel-open');
                                    angular.element('.wrapper').addClass('wrapper-active');
                                    angular.element('body').addClass('window-noscroll');
                                }
                            });
                        }

                    }
                }])
                .directive('input', blurFocusDirective)
                .directive('select', blurFocusDirective)
                .directive('textarea', blurFocusDirective)
                .directive('dummyProgress',['$timeout',function($timeout){
                    return {
                        restrict: 'A',
                        link : function(scope, elem, attr){
                            elem.animate({
                                width:"100%"
                            },function(){
                                $timeout(function(){
                                    elem.parents('.file-item').find('.file-name').css({
                                        'margin-top': '5px'
                                    });
                                    elem.parents('.progress').remove();
                                },1000);
                            });
                        }
                    };
                }]);




        });
})(define);