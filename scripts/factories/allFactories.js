(function (define) {
    "use strict";
    define(['angular'
    ], function (angular) {
        return angular.module('AppXpress.factories', [])
            .factory('AppData', ['localStorageService',
                function (localStorageService) {
                    var storedData  =   localStorageService.get('AppData');
                    return storedData ? storedData : {};
                }
            ]);
    });
})(define);