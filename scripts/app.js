(function(define){
    "use strict";
    define([
        'angular',
        'factories/allFactories',
        'directives/directives',
        'controllers/controllerBootstrap',
        'uiRouter',
        'ngBootstrap',
        'LocalStorageModule',
        'ngBootstrapSlider',
        'textAngular',
        'angularFileUpload'
    ], function (angular) {
        // Declare app level module which depends on filters, and factories

        return angular.module('AppXpress', [
            'ui.router',
            'ui.bootstrap',
            'LocalStorageModule',
            'textAngular',
            'ui.bootstrap-slider',
            'angularFileUpload',
            'AppXpress.factories',
            'AppXpress.directives',
            'AppXpress.controllers'
        ]);


    });
})(define);
