require.config({
    paths: {
        jQuery: '../scripts/bower_components/jquery/dist/jquery.min',
        angular: '../scripts/bower_components/angular/angular.min',
        uiRouter: '../scripts/bower_components/angular-ui-router/release/angular-ui-router',
        'ngBootstrap': '../scripts/bower_components/angular-bootstrap/ui-bootstrap-tpls',
        'LocalStorageModule': '../scripts/bower_components/angular-local-storage/dist/angular-local-storage.min',
        'bootstrapSlider': '../scripts/bower_components/seiyria-bootstrap-slider/dist/bootstrap-slider.min',
        'ngBootstrapSlider':    '../scripts/bower_components/angular-bootstrap-slider/slider',
        'ngSanitize'   :   '../scripts/bower_components/textAngular/src/textAngular-sanitize',
        'textAngularSetup'  : '../scripts/bower_components/textAngular/src/textAngularSetup',
        'textAngular'   :   '../scripts/bower_components/textAngular/src/textAngular',
        'underscore'   :   '../scripts/bower_components/underscore/underscore',
        angularFileUpload: '../scripts/bower_components/ng-file-upload/angular-file-upload.min',
        angularFileUploadShim: '../scripts/bower_components/ng-file-upload/angular-file-upload-shim.min',
        modernizr:  '../scripts/bower_components/modernizr/modernizr'
    },
    shim: {

        'ngBootstrapSlider' : {
            deps:['angular','bootstrapSlider']
        },

        'angularFileUpload': {
            deps: ['angular','angularFileUploadShim'],
            'exports': 'angular.fileUpload'
        },


        'underscore' : {
            exports : '_'
        },

        'ngSanitize' : {
            deps: ['angular']
        },

        'textAngularSetup' : {
            deps: ['angular']
        },

        'textAngular'   :   {
            deps: ['angular', 'ngSanitize','textAngularSetup']
        },


        'jQuery': {
            exports: 'jQuery'
        },
        'angular': {
            deps: ['jQuery','angularFileUploadShim','modernizr'],
            'exports': 'angular'
        },

        'uiRouter': {
            deps: ['angular']
        },
        'ngBootstrap': {
            deps: ['angular'],
            'exports': 'ngBootstrap'
        },
        'LocalStorageModule': {
            deps: ['angular']
        }
    },
    waitSeconds: 60
});

//http://code.angularjs.org/1.2.1/docs/guide/bootstrap#overview_deferred-bootstrap
window.name = "AppXpress";

require([
    'angular',
    'jQuery',
    'app',
    'routeManager'
], function (angular) {
    'use strict';

    angular.element().ready(function () {
        console.log('Starting angular.bootstrap');
        angular.bootstrap(document.getElementsByTagName('html')[0], ['AppXpress']);
    });
});

