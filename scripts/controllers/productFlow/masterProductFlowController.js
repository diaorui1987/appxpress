(function (define) {
    "use strict";

    define(['angular','underscore'], function (angular, _) {

        var masterProductFlowController = function ($scope, $state,  localStorageService, AppData ) {
            console.log("Starting Production Application with 2 steps");

            $scope.maxStep = 2;
            $scope.appType  = 'Production Application';
            AppData.appType = $scope.appType;

            //localStorageService.set('AppData',{});return;
            AppData.completedSteps  =   AppData.completedSteps ? AppData.completedSteps : [];


            //Save the completed step
            $scope.$on('$stateChangeSuccess',function (event, toState) {
                event.preventDefault();
                var currentUrl = toState.url;
                $scope.currentStep =   currentUrl.substring(1);
                if(AppData.completedSteps.length){
                    var lastStep    =   _.max(AppData.completedSteps);
                    if($scope.currentStep > (parseInt(lastStep)+1)) {
                        $state.go('root.productFlow.flow'+(parseInt(lastStep)+1));
                    }
                } else {
                    if($scope.currentStep != 1){
                        $state.go('root.applicationSelection');
                    }
                }
            });

            $scope.goToNextStep =   function(canGoToNext,state){
                if(canGoToNext){
                    //To prevent duplicate entries in completed steps array
                    if(_.indexOf(AppData.completedSteps,$scope.currentStep) < 0){
                        AppData.completedSteps.push($scope.currentStep);
                        localStorageService.set('AppData',AppData);
                    }
                    localStorageService.set('AppData',AppData);
                    $state.go(state);
                }
            };
        };

        // Register as global constructor function
        return [ '$scope', '$state', 'localStorageService', 'AppData', masterProductFlowController];
    });
})(define);
