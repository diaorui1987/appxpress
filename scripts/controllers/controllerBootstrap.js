define(['angular',
        'controllers/templates/defaultBaseController',

        'controllers/productFlow/masterProductFlowController',
        'controllers/productFlow/productFlowOneController',
        'controllers/productFlow/productFlowTwoController',

        'controllers/protoPilotFlow/masterPilotFlowController',
        'controllers/protoPilotFlow/masterProtoFlowController',

        'controllers/protoPilotFlow/protoPilotFlowOneController',
        'controllers/protoPilotFlow/protoPilotFlowTwoController',
        'controllers/protoPilotFlow/protoPilotFlowThreeController',
        'controllers/protoPilotFlow/protoPilotFlowFourController',
        'controllers/protoPilotFlow/protoPilotFlowFiveController',

        'controllers/viewSummaryController',
        'controllers/protoPilotFlow/fullController'
    ],
    function (angular,
              
              defaultBaseController,

              masterProductFlowController,
              productFlowOneController,
              productFlowTwoController,

              masterPilotFlowController,
              masterProtoFlowController,

              protoPilotFlowOneController,
              protoPilotFlowTwoController,
              protoPilotFlowThreeController,
              protoPilotFlowFourController,
              protoPilotFlowFiveController,

              viewSummaryController,
              fullController
              
              ) {
        'use strict';

        return angular.module('AppXpress.controllers', [])
            .controller('defaultBaseController', defaultBaseController)

            .controller('masterProductFlowController', masterProductFlowController)
            .controller('productFlowOneController', productFlowOneController)
            .controller('productFlowTwoController', productFlowTwoController)

            .controller('masterPilotFlowController', masterPilotFlowController)
            .controller('masterProtoFlowController', masterProtoFlowController)

            .controller('protoPilotFlowOneController', protoPilotFlowOneController)
            .controller('protoPilotFlowTwoController', protoPilotFlowTwoController)
            .controller('protoPilotFlowThreeController', protoPilotFlowThreeController)
            .controller('protoPilotFlowFourController', protoPilotFlowFourController)
            .controller('protoPilotFlowFiveController', protoPilotFlowFiveController)

            .controller('viewSummaryController', viewSummaryController)
            .controller('fullController', fullController);

    });
           
