(function (define) {
    "use strict";

    define(['angular'], function (angular) {

        var protoPilotFlowTwoController = function ($scope, AppData) {
            $scope.stateFlow = AppData.stateFlow;

            $scope.minNumStaticPages  =   1;
            $scope.maxNumStaticPages  =   5;
            $scope.noOfStaticPages = AppData.noOfStaticPages || 1;

            $scope.$watch('noOfStaticPages',function(){
                AppData.noOfStaticPages  =   $scope.noOfStaticPages;
            });
        };

        // Register as global constructor function
        return [ '$scope', 'AppData', protoPilotFlowTwoController];
    });
}(define));
