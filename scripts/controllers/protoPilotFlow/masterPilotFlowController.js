(function (define) {
    "use strict";

    define(['angular','underscore'], function (angular, _) {

        var masterProtoPilotFlowController = function ($scope, $state, localStorageService, AppData) {
            console.log("Starting Pilot Application with 5 steps");
            $scope.maxStep = 5;
            $scope.appType = 'Pilot Application';
            AppData.appType = $scope.appType;

            AppData.stateFlow = 'root.pilotFlow';
            $scope.stateFlow = 'root.pilotFlow';

            //localStorageService.set('AppData',{});return;
            AppData.completedSteps  =   AppData.completedSteps ? AppData.completedSteps : [];


            //Save the completed step
            $scope.$on('$stateChangeSuccess',function (event, toState) {
                event.preventDefault();
                var currentUrl = toState.url;
                $scope.currentStep =   currentUrl.substring(1);
                if(AppData.completedSteps.length){
                    var lastStep    =   _.max(AppData.completedSteps);
                    if($scope.currentStep > (parseInt(lastStep)+1)) {
                        $state.go( 'root.pilotFlow.flow'+(parseInt(lastStep)+1));
                    }
                } else {
                    if($scope.currentStep != 1){
                        $scope.currentStep = 1;
                        $state.go( 'root.pilotFlow.flow1');
                    }
                }

            });

            $scope.goToNextStep =   function(canGoToNext,state){
                if(canGoToNext){
                    //To prevent duplicateentries in completed steps array
                    if(_.indexOf(AppData.completedSteps,$scope.currentStep) < 0){
                        AppData.completedSteps.push($scope.currentStep);
                        localStorageService.set('AppData',AppData);
                    }
                    localStorageService.set('AppData',AppData);
                    var nextState = 'root.pilotFlow.' + state;
                    console.log("nextState is...");
                    console.log(nextState);
                    $state.go(  nextState );
                }
            };
        };

        // Register as global constructor function
        return [ '$scope', '$state', 'localStorageService', 'AppData', masterProtoPilotFlowController];
    });
})(define);
