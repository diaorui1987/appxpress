(function (define) {
    "use strict";

    define(['angular','underscore'], function (angular,_) {

        var protoPilotFlowFiveController = function ($scope, $http, $location, $window, localStorageService, AppData) {

            $scope.stateFlow = AppData.stateFlow;

            var objectToArray   =   function(deviceObj){
                var filteredDeviceList    =   [];
                angular.forEach(deviceObj,function(value, key){
                    if(value){
                        filteredDeviceList.push(key);
                    }
                });

                return filteredDeviceList;
            };


            $scope.appDescription  =   AppData.appDescription;
            $scope.noOfStaticPages  =   AppData.noOfStaticPages;
            $scope.appGoal  =   AppData.appGoal;
            $scope.appType  =   AppData.appType;
            $scope.appMainFeatures  =   AppData.appMainFeatures;
            $scope.appUserFlow  =   AppData.appUserFlow;
            $scope.Device  =   objectToArray(AppData.Device);
            $scope.OSes = objectToArray(AppData.OSes);
            $scope.techSavvyLevel  =   AppData.techSavvyLevel;
            $scope.appUserDesc  =   AppData.appUserDesc;
            $scope.uploadedFiles    =  AppData.uploadedFiles;

            $scope.deviceClass =    $scope.Device.sort().join('-');


            $scope.confirmOrder = function() {
                  var baseUrl = $location.protocol() + "://" + $location.host();         
                            var ajaxPromise = $http( {
                                method: 'POST',
                                url: '/restapi/api.php?x=submit',
                                data: AppData
                            }).
                            success( function(data, status, headers, config) {

                                $scope.orderSubmitResponse = data;
                                console.log(data);
				

                                //To remove the form wizard data
                                //localStorageService.remove('AppData');

				document.cookie = "axp_id=" + data["ordernumber"] + "; path=/";
				var url = baseUrl + '/shoppingcart/?axp_id=' + data["ordernumber"];
				//$window.location.href = url;


                            }).
                            error( function(data, status, headers, config) {
                                console.log(data);
                                console.log(status);
                                console.log(headers);
                                console.log(config);
				alert("Error!");
                            });
                    };
    };

        // Register as global constructor function
        return [ '$scope', '$http', '$location', '$window', 'localStorageService', 'AppData', protoPilotFlowFiveController];
    });
}(define));
