(function (define) {
    "use strict";
    /*
    //define(['../../../bower_components/angular/angular.min','underscore'], function (angular, _) 
    */
    define(['angular','underscore'], function (angular, _) {

        var masterProtoPilotFlowController = function ($scope, $state, localStorageService, AppData) {

            $scope.maxStep = 5;
            $scope.appType = 'Design Mockups';
            AppData.appType = $scope.appType;

            console.log("masterProtoPilotFlowController started.");
            console.log("Starting a " + $scope.appType);

            AppData.stateFlow = 'root.protoFlow';
            $scope.stateFlow = AppData.stateFlow;

            //localStorageService.set('AppData',{});return;
            AppData.completedSteps  =   AppData.completedSteps ? AppData.completedSteps : [];


            //Save the completed step
            $scope.$on('$stateChangeSuccess',function (event, toState) {
                event.preventDefault();
                var currentUrl = toState.url;
                $scope.currentStep =   currentUrl.substring(1);
                if(AppData.completedSteps.length){
                    var lastStep    =   _.max(AppData.completedSteps);
                    if($scope.currentStep > (parseInt(lastStep)+1)) {
                        $state.go( 'root.protoFlow.flow'+(parseInt(lastStep)+1));
                    }
                } else {
                    if($scope.currentStep != 1){
                        $scope.currentStep = 1;
                        $state.go( 'root.protoFlow.flow1');
                    }
                }

            });

            $scope.goToNextStep =   function(canGoToNext,state){
                if(canGoToNext){
                    //To prevent duplicateentries in completed steps array
                    if(_.indexOf(AppData.completedSteps,$scope.currentStep) < 0){
                        AppData.completedSteps.push($scope.currentStep);
                        localStorageService.set('AppData',AppData);
                    }
                    localStorageService.set('AppData',AppData);
                    var nextState = 'root.protoFlow.' + state;
                    console.log("nextState is...");
                    console.log(nextState);
                    $state.go(  nextState );
                }
            };
        };

        // Register as global constructor function
        return [ '$scope', '$state', 'localStorageService', 'AppData', masterProtoPilotFlowController];
    });
})(define);
