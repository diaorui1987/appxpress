(function (define) {
    "use strict";

    define(['angular','underscore'], function (angular, _) {

        var fullController = function ($scope, $state, localStorageService, AppData, $upload, $http, $location, $window) {
            console.log("Starting Pilot Application with 5 steps");
            $scope.maxStep = 5;
            $scope.appType = 'Pilot Application';
            AppData.appType = $scope.appType;

            AppData.stateFlow = 'root.pilotFlow';
            $scope.stateFlow = 'root.pilotFlow';

            //localStorageService.set('AppData',{});return;
            AppData.completedSteps  =   AppData.completedSteps ? AppData.completedSteps : [];


            //Save the completed step
            $scope.$on('$stateChangeSuccess',function (event, toState) {
                event.preventDefault();
                var currentUrl = toState.url;
                $scope.currentStep =   currentUrl.substring(1);
                if(AppData.completedSteps.length){
                    var lastStep    =   _.max(AppData.completedSteps);
                    if($scope.currentStep > (parseInt(lastStep)+1)) {
                        $state.go( 'root.pilotFlow.flow'+(parseInt(lastStep)+1));
                    }
                } else {
                    if($scope.currentStep != 1){
                        $scope.currentStep = 1;
                        $state.go( 'root.pilotFlow.flow1');
                    }
                }

            });




            /*  one */
            AppData.Device  =   AppData.Device || {};
            AppData.OSes    =   AppData.OSes || {};

            $scope.stateFlow = AppData.stateFlow;

            $scope.Device    =   {};

            $scope.Device.Phone    = AppData.Device.Phone ? true : false;
            $scope.Device.Tablet   = AppData.Device.Tablet ? true :  false;
            $scope.Device.Desktop    = AppData.Device.Desktop  ? true : false;

            $scope.OSes  = {};
            $scope.OSes['iOS']  =   AppData.OSes['iOS'] ? true : false;
            $scope.OSes['Android']  =   AppData.OSes['Android'] ? true : false;
            $scope.OSes['HTML5']  =   AppData.OSes['HTML5'] ? true : false;
            $scope.OSes['DontKnow']  =   AppData.OSes['DontKnow'] ? true : false;

            $scope.canGoToNext  =   canGoToNext();

            $scope.$watchCollection('Device',function(){
                AppData.Device  =  $scope.Device;
                $scope.canGoToNext  =   canGoToNext();

                if ( AppData.Device.Desktop == true ) { 
                  $scope.OSes['iOS'] = false;
                  $scope.OSes['Android'] = false;
                  $scope.OSes['HTML5'] = false;
                  $scope.toggleCheck('HTML5',null);
                }
            });


            /* two */
            $scope.minNumStaticPages  =   1;
            $scope.maxNumStaticPages  =   5;
            $scope.noOfStaticPages = AppData.noOfStaticPages || 1;

            $scope.$watch('noOfStaticPages',function(){
                AppData.noOfStaticPages  =   $scope.noOfStaticPages;
            });


            /* three */

            $scope.appGoal  =   AppData.appGoal || '';
            $scope.appUserDesc  =   AppData.appUserDesc || '';

            $scope.techSavvyLevel  =   AppData.techSavvyLevel || 5;
            $scope.techSavvyMin =   1;
            $scope.techSavvyMax =   10;


            $scope.appMainFeatures =   AppData.appMainFeatures ? AppData.appMainFeatures: ['','',''];
            $scope.appUserFlow =   AppData.appUserFlow ? AppData.appUserFlow: ['','',''];

            $scope.appMainFeaturesPlaceholder = ['e.g., Access Hospitals','',''];
            $scope.appUserFlowPlaceholder     = ['e.g., Access schedules','',''];

            $scope.removeTextBox   =   function(index, arrayType){
                switch(arrayType){
                    case 1: // features
                        $scope.appMainFeatures.splice(index, 1);
                        break;
                    case 2: // flows
                        $scope.appUserFlow.splice(index, 1);
                        break;
                }
            };

            var maxTextboxes    =   15;

            $scope.addTextBox   =   function(arrayType){
                switch(arrayType){
                    case 1: // features
                        if($scope.appMainFeatures.length < maxTextboxes){
                            $scope.appMainFeatures.push('');
                            $scope.appMainFeaturesPlaceholder.push('');
                        }

                        break;
                    case 2: // flows
                        if($scope.appUserFlow.length < maxTextboxes){
                            $scope.appUserFlow.push('');
                            $scope.appUserFlowPlaceholder.push('');
                        }
                        break;
                }
            };


            function canGoToNext() {
                return true;
            };


            $scope.updateAppSpec    =   function(){
                AppData.techSavvyLevel   =   $scope.techSavvyLevel;
                AppData.appMainFeatures   =   $scope.appMainFeatures;
                AppData.appUserFlow   =   $scope.appUserFlow;
                AppData.appGoal   =   $scope.appGoal;
                AppData.appUserDesc   =   $scope.appUserDesc;

                $scope.goToNextStep($scope.appSpecForm.$valid,  'flow4');
            };

            /* four */

            $scope.showProgress =   false;
            var prepareUploadedFileForDisplay    =   function(fileArray){

                var getFileType =   function(fileName){
                    var brokenFileName  =   fileName.split('.');
                    var fileType    =_.last(brokenFileName);

                    var finalFileType;
                    switch(fileType){
                        case 'zip':
                        case 'ZIP':
                            finalFileType   =   'zip';
                            break;
                        case 'PDF':
                        case 'DOC':
                        case 'DOCX':
                        case 'TXT':
                        case 'RTF':
                        case 'HTML':
                        case 'pdf':
                        case 'doc':
                        case 'docx':
                        case 'txt':
                        case 'rtf':
                        case 'html':
                            finalFileType   =   'docx';
                            break;
                        case 'png':
                        case 'jpg':
                        case 'gif':
                        case 'psd':
                        case 'PNG':
                        case 'JPG':
                        case 'GIF':
                        case 'PSD':
                            finalFileType   =   'img';
                            break;

                        default:
                            finalFileType   =   'other';
                    }

                    return finalFileType;
                };
                var fileArrayReturned = [];

                angular.forEach(fileArray, function(value, key){

                    var fileObject  =   {};

                    fileObject.fileType =   getFileType(value.name);
                    fileObject.name =   value.name;
                    fileArrayReturned.push(fileObject);

                });

                return fileArrayReturned;
            };
            $scope.appDescription   =   AppData.appDescription ? AppData.appDescription : '';
            $scope.uploadedFiles   =   AppData.uploadedFiles ? AppData.uploadedFiles : false;

            $scope.onFileSelect =   function($files){

                $scope.uploadedFiles    =   $scope.uploadedFiles ? $scope.uploadedFiles : [];
                $scope.showProgress =   true;
                var tempFiles = [];
                for ( var ind = 0; ind < $files.length; ind++ ) {
                    var tempFile = $files[ind];
                    if(tempFile.size != 0)
                        tempFiles.push(tempFile);
                }
                $scope.uploadedFiles = _.union(prepareUploadedFileForDisplay(tempFiles),$scope.uploadedFiles);

                for ( var i = 0; i < $files.length; i++ ) {
                    var file = $files[i];
                    if(file.size == 0)
                        alert('The file \''+ file.name + '\' is empty! Please check the file and upload again.');
                    else {
                        $scope.upload = $upload.upload({
                            url: '/restapi/api.php?x=upload',
                            method: 'POST',
                                        data: {
                                            'key' : file.name,
                                            'acl' : 'public-read',
                                            'Content-Type' : file.type,
                                        },
                            file: file,
                        }).progress(function(evt) {
                            console.log('percent:' + parseInt(100.0 * evt.loaded / evt.total));
                        }).success(function(data, status, headers,config ) {
                            console.log(data);
                        }).error(function(err) {
                            console.log(err);
                        });
                    }
                }

            };

            $scope.removeFile =   function(index){
                $scope.uploadedFiles.splice(index,1);
            };


            $scope.$watchCollection('uploadedFiles',function(){
                AppData.uploadedFiles =   $scope.uploadedFiles;
                localStorageService.set('AppData', AppData);
            });


            $scope.updateData  =   function(route){
                AppData.appDescription   =   $scope.appDescription;
                AppData.uploadedFiles =   $scope.uploadedFiles;
                $scope.goToNextStep($scope.appDescriptionForm.$valid, route);
            };

            /* five */

            var objectToArray   =   function(deviceObj){
                var filteredDeviceList    =   [];
                angular.forEach(deviceObj,function(value, key){
                    if(value){
                        filteredDeviceList.push(key);
                    }
                });

                return filteredDeviceList;
            };


            $scope.appDescription  =   AppData.appDescription;
            $scope.noOfStaticPages  =   AppData.noOfStaticPages;
            $scope.appGoal  =   AppData.appGoal;
            $scope.appType  =   AppData.appType;
            $scope.appMainFeatures  =   AppData.appMainFeatures;
            $scope.appUserFlow  =   AppData.appUserFlow;
            $scope.Device  =   objectToArray(AppData.Device);
            $scope.OSes = objectToArray(AppData.OSes);
            $scope.techSavvyLevel  =   AppData.techSavvyLevel;
            $scope.appUserDesc  =   AppData.appUserDesc;
            $scope.uploadedFiles    =  AppData.uploadedFiles;

            $scope.deviceClass =    $scope.Device.sort().join('-');


            $scope.confirmOrder = function() {
                  var baseUrl = $location.protocol() + "://" + $location.host();         
                            var ajaxPromise = $http( {
                                method: 'POST',
                                url: '/restapi/api.php?x=submit',
                                data: AppData
                            }).
                            success( function(data, status, headers, config) {
                                //console.log(data);
                                //console.log(status);
                                //console.log(headers);
                                //console.log(config);
                                //
                                $scope.orderSubmitResponse = data;
                                console.log(data);
                

                                //To remove the form wizard data
                                localStorageService.remove('AppData');

                document.cookie = "axp_id=" + data["ordernumber"] + "; path=/";
                var url = baseUrl + '/shoppingcart/?axp_id=' + data["ordernumber"];
                $window.location.href = url;


                            }).
                            error( function(data, status, headers, config) {
                                console.log(data);
                                console.log(status);
                                console.log(headers);
                                console.log(config);
                alert("Error!");
                            });
                    };

        }; // end of function

        // Register as global constructor function
        // $scope, $state, localStorageService, AppData, $upload, $http, $location, $window
        return [ '$scope', '$state', 'localStorageService', 'AppData', '$upload', '$http', '$location', '$window', fullController];
    });
})(define);
