(function (define) {
    "use strict";

    define(['angular','underscore'], function (angular, _) {

        var protoPilotFlowFourController = function ($scope, $upload, AppData, localStorageService) {
            $scope.stateFlow = AppData.stateFlow;

            $scope.showProgress =   false;
            var prepareUploadedFileForDisplay    =   function(fileArray){

                var getFileType =   function(fileName){
                    var brokenFileName  =   fileName.split('.');
                    var fileType    =_.last(brokenFileName);

                    var finalFileType;
                    switch(fileType){
                        case 'zip':
                        case 'ZIP':
                            finalFileType   =   'zip';
                            break;
                        case 'PDF':
                        case 'DOC':
                        case 'DOCX':
                        case 'TXT':
                        case 'RTF':
                        case 'HTML':
                        case 'pdf':
                        case 'doc':
                        case 'docx':
                        case 'txt':
                        case 'rtf':
                        case 'html':
                            finalFileType   =   'docx';
                            break;
                        case 'png':
                        case 'jpg':
                        case 'gif':
                        case 'psd':
                        case 'PNG':
                        case 'JPG':
                        case 'GIF':
                        case 'PSD':
                            finalFileType   =   'img';
                            break;

                        default:
                            finalFileType   =   'other';
                    }

                    return finalFileType;
                };
                var fileArrayReturned = [];

                angular.forEach(fileArray, function(value, key){

                    var fileObject  =   {};

                    fileObject.fileType =   getFileType(value.name);
                    fileObject.name =   value.name;
                    fileArrayReturned.push(fileObject);

                });

                return fileArrayReturned;
            };
            $scope.appDescription   =   AppData.appDescription ? AppData.appDescription : '';
            $scope.uploadedFiles   =   AppData.uploadedFiles ? AppData.uploadedFiles : false;

            $scope.onFileSelect =   function($files){

                $scope.uploadedFiles    =   $scope.uploadedFiles ? $scope.uploadedFiles : [];
                $scope.showProgress =   true;
                var tempFiles = [];
                for ( var ind = 0; ind < $files.length; ind++ ) {
                    var tempFile = $files[ind];
                    if(tempFile.size != 0)
                        tempFiles.push(tempFile);
                }
                $scope.uploadedFiles = _.union(prepareUploadedFileForDisplay(tempFiles),$scope.uploadedFiles);

        		for ( var i = 0; i < $files.length; i++ ) {
        			var file = $files[i];
        			if(file.size == 0)
                        alert('The file \''+ file.name + '\' is empty! Please check the file and upload again.');
                    else {
            			$scope.upload = $upload.upload({
            				url: '/restapi/api.php?x=upload',
            				method: 'POST',
                                        data: {
                                            'key' : file.name,
                                            'acl' : 'public-read',
                                            'Content-Type' : file.type,
                                        },
            				file: file,
            			}).progress(function(evt) {
            				console.log('percent:' + parseInt(100.0 * evt.loaded / evt.total));
            			}).success(function(data, status, headers,config ) {
            				console.log(data);
            			}).error(function(err) {
            				console.log(err);
            			});
        			}
        		}

            };

            $scope.removeFile =   function(index){
                $scope.uploadedFiles.splice(index,1);
            };


            $scope.$watchCollection('uploadedFiles',function(){
                AppData.uploadedFiles =   $scope.uploadedFiles;
                localStorageService.set('AppData', AppData);
            });


            $scope.updateData  =   function(route){
                AppData.appDescription   =   $scope.appDescription;
                AppData.uploadedFiles =   $scope.uploadedFiles;
                $scope.goToNextStep($scope.appDescriptionForm.$valid, route);
            };


        };

        // Register as global constructor function
        return [ '$scope', '$upload', 'AppData', 'localStorageService', protoPilotFlowFourController];
    });
}(define));
