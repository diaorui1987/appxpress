(function (define) {
    "use strict";

    define(['angular'], function (angular) {

        var protoPilotFlowOneController = function ($scope, AppData) {
            console.log("protoPilotFlowOneController started");
            console.log("Step 1");

            var canGoToNext =   function(){
                var deviceTypeFlag  =   false;

                //checking that atleast one device is selected
                angular.forEach($scope.Device,function(value, key){
                    if(value){
                        deviceTypeFlag  =   true;
                    }
                });

                return deviceTypeFlag; //&& $scope.orientationType;
            };

            AppData.Device  =   AppData.Device || {};
            AppData.OSes    =   AppData.OSes || {};

            $scope.stateFlow = AppData.stateFlow;

            $scope.Device    =   {};

            $scope.Device.Phone    = AppData.Device.Phone ? true : false;
            $scope.Device.Tablet   = AppData.Device.Tablet ? true :  false;
            $scope.Device.Desktop    = AppData.Device.Desktop  ? true : false;

            $scope.OSes  = {};
            $scope.OSes['iOS']  =   AppData.OSes['iOS'] ? true : false;
            $scope.OSes['Android']  =   AppData.OSes['Android'] ? true : false;
            $scope.OSes['HTML5']  =   AppData.OSes['HTML5'] ? true : false;
            $scope.OSes['DontKnow']  =   AppData.OSes['DontKnow'] ? true : false;

            $scope.canGoToNext  =   canGoToNext();

            $scope.$watchCollection('Device',function(){
                AppData.Device  =  $scope.Device;
                $scope.canGoToNext  =   canGoToNext();

                if ( AppData.Device.Desktop == true ) { 
                  $scope.OSes['iOS'] = false;
                  $scope.OSes['Android'] = false;
                  $scope.OSes['HTML5'] = false;
                  $scope.toggleCheck('HTML5',null);
                }
            });

/*
            $scope.$watchCollection('OSes', function(){
                AppData.OSes = $scope.OSes;
                //$scope.canGoToNext  = canGoToNext();
            });
*/

	    $scope.toggleCheck = function(x,e) {

               if (e) {
                 e.stopPropagation();
                 e.preventDefault();
               }

               if ( AppData.Device.Desktop == true ) {
                 $scope.OSes['iOS'] = false;
                 $scope.OSes['Android'] = false;

                 if ( x == 'HTML5' ) {
                    $scope.OSes['HTML5'] = true;
                    $scope.OSes['DontKnow'] = false;
                 }
                 else if ( x == 'DontKnow' ) {
                    $scope.OSes['HTML5'] = false;
                    $scope.OSes['DontKnow'] = true;
                 }
               }
               else {
                 if ( $scope.OSes[x] === false ) {
                    $scope.OSes[x] = true;
                 } 
                 else {
                    $scope.OSes[x] = false;
                 }
               }

               if ( x == 'DontKnow' ) { 
                 if ( $scope.OSes['DontKnow'] == true ) {
                   $scope.OSes['iOS'] = false;
                   $scope.OSes['Android'] = false;
                   $scope.OSes['HTML5'] = false;
                 }
               }
               else {
                 if ( $scope.OSes['DontKnow'] == true ) {
                   if ( $scope.OSes['iOS'] || $scope.OSes['Android'] || $scope.OSes['HTML5'] ) {
                      $scope.OSes['DontKnow'] = false;
                   } 
                 }
               }

               if ( x == 'iOS' && $scope.OSes['iOS'] ) {
                 $scope.OSes['Android'] = false;
                 $scope.OSes['HTML5'] = false;
               }

               if ( x == 'Android' && $scope.OSes['Android'] ) {
                 $scope.OSes['iOS'] = false;
                 $scope.OSes['HTML5'] = false;
               }

               if ( x == 'HTML5' && $scope.OSes['HTML5'] ) {
                 $scope.OSes['iOS'] = false;
                 $scope.OSes['Android'] = false;
               }



               for ( var key in $scope.OSes ) { 
                  AppData.OSes[key] = $scope.OSes[key];
               }

            };

            $scope.isChecked = function(x) {
               ////console.log('isChecked called');
               //console.log(x);
               //console.log(AppData.OSes[x]);
               return AppData.OSes[x];
            };


        };

        // Register as global constructor function
        return [ '$scope', 'AppData', protoPilotFlowOneController];
    });
}(define));
