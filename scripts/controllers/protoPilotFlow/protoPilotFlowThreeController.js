(function (define) {
    "use strict";

    define(['angular'], function (angular) {

        var protoPilotFlowThreeController = function ($scope, AppData) {

            $scope.stateFlow = AppData.stateFlow;

            $scope.appGoal  =   AppData.appGoal || '';
            $scope.appUserDesc  =   AppData.appUserDesc || '';

            $scope.techSavvyLevel  =   AppData.techSavvyLevel || 5;
            $scope.techSavvyMin =   1;
            $scope.techSavvyMax =   10;


            $scope.appMainFeatures =   AppData.appMainFeatures ? AppData.appMainFeatures: ['','',''];
            $scope.appUserFlow =   AppData.appUserFlow ? AppData.appUserFlow: ['','',''];

            $scope.appMainFeaturesPlaceholder = ['e.g., Access Hospitals','',''];
            $scope.appUserFlowPlaceholder     = ['e.g., Access schedules','',''];

            $scope.removeTextBox   =   function(index, arrayType){
                switch(arrayType){
                    case 1: // features
                        $scope.appMainFeatures.splice(index, 1);
                        break;
                    case 2: // flows
                        $scope.appUserFlow.splice(index, 1);
                        break;
                }
            };

            var maxTextboxes    =   15;

            $scope.addTextBox   =   function(arrayType){
                switch(arrayType){
                    case 1: // features
                        if($scope.appMainFeatures.length < maxTextboxes){
                            $scope.appMainFeatures.push('');
                            $scope.appMainFeaturesPlaceholder.push('');
                        }

                        break;
                    case 2: // flows
                        if($scope.appUserFlow.length < maxTextboxes){
                            $scope.appUserFlow.push('');
                            $scope.appUserFlowPlaceholder.push('');
                        }
                        break;
                }
            };




            $scope.updateAppSpec    =   function(){
                AppData.techSavvyLevel   =   $scope.techSavvyLevel;
                AppData.appMainFeatures   =   $scope.appMainFeatures;
                AppData.appUserFlow   =   $scope.appUserFlow;
                AppData.appGoal   =   $scope.appGoal;
                AppData.appUserDesc   =   $scope.appUserDesc;

                $scope.goToNextStep($scope.appSpecForm.$valid,  'flow4');
            };
        };

        // Register as global constructor function
        return [ '$scope', 'AppData', protoPilotFlowThreeController];
    });
}(define));
