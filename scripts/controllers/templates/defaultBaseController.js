(function (define) {
    "use strict";

    define(['angular'], function (angular) {

        var defaultBaseController = function ($http, $scope, $stateParams ) {

           console.log("Called defaultBaseController");

        };

        // Register as global constructor function
        return ['$http', '$scope',  '$stateParams', defaultBaseController];
    });
}(define));
