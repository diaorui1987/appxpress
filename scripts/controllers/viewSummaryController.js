(function (define) {
    "use strict";

    define(['angular','underscore'], function (angular,_) {

        var viewSummaryController = function ($scope, $stateParams, $http ) {
            console.log('viewSummaryController started.');

            var objectToArray   =   function(deviceObj){
                var filteredDeviceList    =   [];
                angular.forEach(deviceObj,function(value, key){
                    if(value){
                        filteredDeviceList.push(key);
                    }
                });

                return filteredDeviceList;
            };

            $scope.retrieveOrder = function(ordernumber) {
		var postData = {};
		postData['apx_id'] = ordernumber;

                var ajaxPromise = $http( {
			method: 'POST',
			url: 'https://appxpress.com/restapi/api.php?x=retrieve',
			data: postData
		})
		.success( function(response, status, headers, config) {
			console.log(response);
			var data = response["response"];
            		$scope.appDescription  =   data.appDescription;
            		$scope.noOfStaticPages  =   data.noOfStaticPages;
            		$scope.appGoal  =   data.appGoal;
            		$scope.appType  =   data.appType;
            		$scope.appUserDesc  =   data.appUserDesc;

            		$scope.appMainFeatures  =   JSON.parse(data.appMainFeatures);
            		$scope.appUserFlow  =   JSON.parse(data.appUserFlow);
		
            		$scope.Device  =   objectToArray(JSON.parse(data.Device));
            		$scope.OSes = objectToArray(JSON.parse(data.OSes));

            		$scope.uploadedFiles    =  JSON.parse(data.uploadedFiles);
            		$scope.deviceClass =    $scope.Device.sort().join('-');
		})

                            .error( function(data, status, headers, config) {
                                console.log(data);
                                console.log(status);
                                console.log(headers);
                                console.log(config);
				alert("Error!");
                            });
		};

		$scope.retrieveOrder($stateParams['ordernumber']);
    };

        // Register as global constructor function
        return [ '$scope', '$stateParams', '$http', viewSummaryController ];
    });
}(define));
