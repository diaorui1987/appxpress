(function (define) {
    "use strict";
    define(['app' , 'uiRouter', 'angular' ], function (app) {

        return app.config(['$urlRouterProvider', '$stateProvider', '$compileProvider', function ($urlRouterProvider, $stateProvider, $compileProvider ) {
            console.log('starting config block ');
            
            $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|file|tel):/); 
            $compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|ftp|mailto|file|tel):/);

            $urlRouterProvider
                .when('', '/protoFlow/1');

            $urlRouterProvider
                .otherwise('/404');

            $stateProvider.state('404', {
                url: '/404',
            });

            $stateProvider.state('root', {
                url: '',
                controller: 'defaultBaseController',
                templateUrl: '../views/templates/main_template.html',

            });

            $stateProvider.state('root.full', {
                url: '/full',
                controller: 'fullController',
                templateUrl: '../views/protoPilotFlow/full.html',

            });            


            $stateProvider.state('root.viewSummary', {
        		url: '/viewSummary/:ordernumber',
        		templateUrl: '../views/templates/viewSummary.html',
        		controller: 'viewSummaryController'
            });

            $stateProvider
                .state('root.productFlow', {
                    url: '/productFlow',
                    templateUrl: '../views/productFlow/masterProductFlow.html',
                    controller: 'masterProductFlowController'
                })
                .state('root.productFlow.flow1', {
                    url: '/1',
                    templateUrl: '../views/productFlow/1.html',
                    controller: 'productFlowOneController'
                })
                .state('root.productFlow.flow2', {
                    url: '/2',
                    templateUrl: '../views/productFlow/2.html',
                    controller: 'productFlowTwoController'
                })


            $stateProvider
                .state('root.protoFlow', {
                    url: '/protoFlow',
                    templateUrl: '../views/protoPilotFlow/masterProtoPilotFlow.html',
                    controller: 'masterProtoFlowController',
                })
                .state('root.protoFlow.flow1', {
                    url: '/1',
                    templateUrl: '../views/protoPilotFlow/1.html',
                    controller: 'protoPilotFlowOneController',
                })
                .state('root.protoFlow.flow2', {
                    url: '/2',
                    templateUrl: '../views/protoPilotFlow/2.html',
                    controller: 'protoPilotFlowTwoController'
                })
                .state('root.protoFlow.flow3', {
                    url: '/3',
                    templateUrl: '../views/protoPilotFlow/3.html',
                    controller: 'protoPilotFlowThreeController'
                })
                .state('root.protoFlow.flow4', {
                    url: '/4',
                    templateUrl: '../views/protoPilotFlow/4.html',
                    controller: 'protoPilotFlowFourController'
                })
                .state('root.protoFlow.flow5', {
                    url: '/5',
                    templateUrl: '../views/protoPilotFlow/5.html',
                    controller: 'protoPilotFlowFiveController'
                });

            $stateProvider
                .state('root.pilotFlow', {
                    url: '/pilotFlow',
                    templateUrl: '../views/protoPilotFlow/masterProtoPilotFlow.html',
                    controller: 'masterPilotFlowController'
                })
                .state('root.pilotFlow.flow1', {
                    url: '/1',
                    templateUrl: '../views/protoPilotFlow/1.html',
                    controller: 'protoPilotFlowOneController'
                })
                .state('root.pilotFlow.flow2', {
                    url: '/2',
                    templateUrl: '../views/protoPilotFlow/2.html',
                    controller: 'protoPilotFlowTwoController'
                })
                .state('root.pilotFlow.flow3', {
                    url: '/3',
                    templateUrl: '../views/protoPilotFlow/3.html',
                    controller: 'protoPilotFlowThreeController'
                })
                .state('root.pilotFlow.flow4', {
                    url: '/4',
                    templateUrl: '../views/protoPilotFlow/4.html',
                    controller: 'protoPilotFlowFourController'
                })
                .state('root.pilotFlow.flow5', {
                    url: '/5',
                    templateUrl: '../views/protoPilotFlow/5.html',
                    controller: 'protoPilotFlowFiveController'
                });

        }])
    });

})(define);
